
# mc.skript.pl - konfiguracja

## Repozytorium

**Issues:**
- Propozycje załączenia nowych plików w repozytorium.

**Merge requests:**
- Propozycje konkretnych zmian w plikach wraz z opisem ich celu.

## Wiadomości

- Wynik komendy: `&6Serwer»&7 Przykładowy wynik komendy &e{zmienna}&7.`
- Błąd komendy: `&6Serwer»&4 Przykładowy błąd komendy &c{zmienna}&4.`
